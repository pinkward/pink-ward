"""
This is the main file for pinkward. It defines the routes and methods for
connecting to the database. Run this file via flask to run the app
"""
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash
import psycopg2 as postgres

#create an app and name it
app = Flask('Pink Ward')

# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE='postgres',
    DEBUG=True,
    USERNAME='postgres',
    PASSWORD='jedimindtrick'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

def connect_db():
    #connect to the database
    conn_string = "host='localhost' dbname='pinkward' user='postgres' password='jedimindtrick'"
    return postgres.connect(conn_string)

def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.db = connect_db()
    return g.db

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

@app.route('/')
def home_display():
    return render_template('pinkward.html')

@app.route('/directory', methods=['GET','POST'])
def directory_display():
    cur = g.db.cursor()

    #getting a list of households
    cur.execute("SELECT DISTINCT address FROM Wardmember")
    households = list()
    for row in cur.fetchall():
        households.append(row[0])

    #getting a list of committees
    cur.execute("SELECT DISTINCT committee FROM Wardmember")
    committees = list()
    for row in cur.fetchall():
        committees.append(row[0])

    #getting a list of fhe families
    cur.execute("SELECT * FROM fhefamily ORDER BY familyid")
    fhe = list()
    for row in cur.fetchall():
        fhe.append(row[0])

    header_msg = ""
    #handling filter request with post
    if request.method == 'POST':
        #--household filter--
        if request.form['filter'] == 'household':
            filter_query = "SELECT * FROM WardMember WHERE address = %s;"
            cur.execute(filter_query, [request.form['household']])
            header_msg = request.form['household']
            people = [dict(name=row[2]+" "+row[3], pic=row[6], phone=row[4], email=row[5], address=row[7], calling=row[8]) for row in cur.fetchall()]
            return render_template('directory.html', people=people, households=households, fhe=fhe, committees=committees, header=header_msg)
        #--committee filter--
        if request.form['filter'] == 'committee':
            filter_query = "SELECT * FROM WardMember WHERE committee = %s;"
            cur.execute(filter_query, [request.form['committee']])
            header_msg = request.form['committee']
            people = [dict(name=row[2]+" "+row[3], pic=row[6], phone=row[4], email=row[5], address=row[7], calling=row[8]) for row in cur.fetchall()]
            return render_template('directory.html', people=people, households=households, fhe=fhe, committees=committees, header=header_msg)
        #--FHE Family Filter--
        if request.form['filter'] == 'fhefamily':
            filter_query = """SELECT * FROM wardmember JOIN fhefamily ON wardmember.address = fhefamily.address1 or wardmember.address = fhefamily.address2
 or wardmember.address = fhefamily.address3 or wardmember.address = fhefamily.address4 WHERE familyid = %s;"""
            cur.execute(filter_query, [request.form['fhefamily']])
            header_msg = 'FHE Group ' + request.form['fhefamily']
            people = [dict(name=row[2]+" "+row[3], pic=row[6], phone=row[4], email=row[5], address=row[7], calling=row[8]) for row in cur.fetchall()]
            return render_template('directory.html', people=people, households=households, fhe=fhe, committees=committees, header=header_msg)

    #getting everyone in the ward
    header_msg = "Ward Directory"
    cur.execute("SELECT * FROM WardMember ORDER BY memberid;")
    people = [dict(name=row[2]+" "+row[3], pic=row[6], phone=row[4], email=row[5], address=row[7], calling=row[8]) for row in cur.fetchall()]
    return render_template('directory.html', people=people, households=households, fhe=fhe, committees=committees, header=header_msg)

@app.route('/activities', methods=['GET', 'POST'])
def activity_display():
    cur = g.db.cursor()

    if request.method == 'POST':
        #Marking yourself as attending an activity
        if request.form['action'] == 'mark_attending':
            query = ("SELECT attending FROM Activity WHERE name=%s AND date=%s;")
            cur.execute(query, [request.form['activity_name'], request.form['activity_date']])
            for row in cur.fetchall():
                x = row[0]
            x = x+1
            query = ("UPDATE Activity SET attending=%s WHERE name=%s AND date=%s")
            cur.execute(query, [x, request.form['activity_name'], request.form['activity_date']])
            g.db.commit()

        #Creating a new activity
        if request.form['action'] =='add_activity':
            add_query = "INSERT INTO Activity (name, date, wardid, committee, attending, description, location) VALUES (%s, %s, 1, %s, 1, %s, %s);"
            #activityinfo = request.form['activityname'] + ', ' + request.form['activitydate'] + ', '  + request.form['activitycommittee']
            try:
                #print cur.mogrify(add_query, (request.form['activityname'], request.form['activitydate'], request.form['activitycommittee'], request.form['activitydescription'], request.form['activitylocation']))
                cur.execute(add_query, (request.form['activityname'], request.form['activitydate'], request.form['activitycommittee'], request.form['activitydescription'], request.form['activitylocation']))
                g.db.commit()
            except:
                print "query didn't work"
                print request.form

    cur.execute("SELECT * FROM Activity WHERE date>CURRENT_TIMESTAMP ORDER BY date;")
    activities = [dict(name=row[0], date=row[1], location=row[6], description=row[5], attending=row[4]) for row in cur.fetchall()]
    return render_template('activities.html', activities=activities)

@app.route('/teaching', methods=['GET', 'POST'])
def teaching_display():
    cur = g.db.cursor()
    #getting a list of households
    cur.execute("SELECT DISTINCT address FROM Wardmember")
    households = list()
    for row in cur.fetchall():
        households.append(row[0])
    #checking for POST
    if request.method == 'POST':
        #request for who do i teach
        if request.form['question'] == 'iteach':
            names =  list(request.form['teacher'].split())
            first = names[0]
            last = names[1]
            #huge query for recursive relation
            teach_query = """SELECT b.firstname, b.lastname, b.phone, b.address, b.photo, b.calling, b.email FROM wardmember AS a
JOIN teaching ON a.memberid = teaching.member1 OR a.memberid = teaching.member2
JOIN wardmember AS b ON teaching.household = b.address WHERE a.firstname = %s AND a.lastname = %s AND a.address = %s;"""
            cur.execute(teach_query,[first,last,request.form['household']])
            #stick it in a dict and send it to the page
            teachees = [dict(name=row[0] + ' ' +row[1], phone=row[2], address=row[3], pic=row[4], calling=row[5], email=row[6]) for row in cur.fetchall()]
            form = 'iteach'
            return render_template('teaching.html', households=households, teachees=teachees, form=form)
        #request for who teaches me
        if request.form['question'] == 'teachme':
            names =  list(request.form['teachee'].split())
            first = names[0]
            last = names[1]
            teach_query = """SELECT a.firstname, a.lastname, a.phone, a.address, a.photo, a.calling, a.email FROM wardmember AS a
JOIN teaching ON a.memberid = teaching.member1 OR a.memberid = teaching.member2
JOIN wardmember AS b ON teaching.household = b.address WHERE b.firstname = %s AND b.lastname = %s AND b.address = %s;"""
            cur.execute(teach_query,[first,last,request.form['household']])
            #stick it in a dict and send it to the page
            teachers = [dict(name=row[0] + ' ' +row[1], phone=row[2], address=row[3], pic=row[4], calling=row[5], email=row[6]) for row in cur.fetchall()]
            form = 'teachme'
            return render_template('teaching.html', households=households, teachers=teachers, form=form)
    #if no request just render the page
    return render_template('teaching.html',households=households)

@app.route('/group', methods=['GET', 'POST'])
def group_display():
    cur = g.db.cursor()

    if request.method == 'POST':
        #Creating a group request
        if request.form['action'] == 'add_group':
            query = "INSERT INTO Groups (name, description) VALUES (%s, %s);"
            cur.execute(query, [request.form['groupname'], request.form['groupdescription']])
            g.db.commit()
            
    cur.execute("SELECT * FROM Groups")
    groups = [dict(name=row[1], description=row[2]) for row in cur.fetchall()]
    #Could be used to count the number of people in a group
    #cur.execute("SELECT * FROM Groups LEFT JOIN inGroup ON Groups.groupid=inGroup.groupid")
    return render_template('scriptures.html', groups=groups)

@app.route('/group/<groupname>', methods=['GET', 'POST'])
def single_group_display(groupname):
    cur = g.db.cursor()

    try:
        query = "SELECT groupid FROM Groups WHERE name=%s"
        cur.execute(query, [groupname])
        for row in cur.fetchall():
            gruid = row[0]
    except:
        print "error while finding groupid"

    if request.method == 'POST':

        #Joining a group

        if request.form['question'] == 'join':
            name = list(request.form['joinname'].split())
            first = name[0]
            last = name[1]
            try:
                query = "SELECT memberid FROM WardMember WHERE firstname=%s AND lastname=%s"
                cur.execute(query, [first, last])
                for row in cur.fetchall():
                    memid = row[0]
            except:
                print "error while finding memid"
            try:
                query = "INSERT INTO inGroup (memberid, groupid) VALUES (%s, %s);"
                cur.execute(query, [memid, gruid])
                g.db.commit()
            except:
                print "error at 3"

        #Adding a new post

        if request.form['question'] == 'new_post':
            name = list(request.form['post_name'].split())
            first = name[0]
            last = name[1]
            #Separate the comment from the tags
            contents = list(request.form['post_contents'].split(' #'))
            comment = contents[0]
            tags = list()
            for x in contents:
                if x != contents[0]:
                    tags.append(x)
            try:
                query = "SELECT memberid FROM WardMember WHERE firstname=%s AND lastname=%s"
                cur.execute(query, [first, last])
                for row in cur.fetchall():
                    memid = row[0]
            except:
                print "error while finding memid"
            #Insert post to database
            try:
                print "got here"
                query = "INSERT INTO Posts (timestamp, memberid, groupid, comment, scripture) VALUES (CURRENT_TIMESTAMP, %s, %s, %s, %s);"
                cur.execute(query, [memid, gruid, comment, request.form['post_scripture']])
                #print cur.mogrify(query, [memid, gruid, comment, request.form['post_scripture']])
                g.db.commit()
                for tag in tags:
                    query = "INSERT INTO ScriptureTags (scripture, tag) VALUES (%s, %s);"
                    cur.execute(query, [request.form['post_scripture'], tag])
                g.db.commit()
            except:
                print "error adding post"

        #Filtering by tag

        if request.form['question'] == 'filter_by_tag':
            query = ("""SELECT DISTINCT firstname, lastname, photo, date_trunc('second', timestamp), comment, Posts.scripture, tag, groupid FROM WardMember INNER JOIN Posts
ON WardMember.memberid=Posts.memberid INNER JOIN ScriptureTags ON Posts.scripture=ScriptureTags.scripture WHERE groupid=%s AND tag=%s;""")
            cur.execute(query, [gruid, request.form['filter_tag']])
            #print cur.mogrify(query, [gruid, request.form['filter_tag']])
            posts = [dict(name= row[0] + ' ' + row[1], photo= row[2], time=row[3], text=row[4], scripture=row[5]) for row in cur.fetchall()]
            for post in posts:
                post.setdefault('tags',list())
                query = ("""SELECT tag FROM WardMember INNER JOIN Posts ON WardMember.memberid=Posts.memberid INNER JOIN ScriptureTags ON
Posts.scripture=ScriptureTags.scripture WHERE groupid=%s AND ScriptureTags.scripture=%s;""")
                cur.execute(query, [gruid, post['scripture']])
                for row in cur.fetchall():
                    post['tags'].append(row[0])
            return render_template('group.html', groupname=groupname, posts=posts)

    #Get the information needed for each post
    query = ("""SELECT firstname, lastname, photo, date_trunc('second', timestamp), comment, scripture FROM WardMember INNER JOIN Posts
ON WardMember.memberid=Posts.memberid WHERE groupid=%s;""")
    cur.execute(query, [gruid])
    posts = [dict(name= row[0] + ' ' + row[1], photo= row[2], time=row[3], text=row[4], scripture=row[5]) for row in cur.fetchall()]

    #Tag posts
    #print posts

    for post in posts:
        post.setdefault('tags',list())
        query = ("""SELECT tag FROM WardMember INNER JOIN Posts ON WardMember.memberid=Posts.memberid INNER JOIN ScriptureTags ON
Posts.scripture=ScriptureTags.scripture WHERE groupid=%s AND ScriptureTags.scripture=%s;""")
        cur.execute(query, [gruid, post['scripture']])
        for row in cur.fetchall():
            post['tags'].append(row[0])

    #Get group members photos
    query = ("SELECT firstname, lastname, photo FROM WardMember INNER JOIN inGroup ON WardMember.memberid=inGroup.memberid WHERE groupid=%s;")
    cur.execute(query, [gruid])
    photos = [dict(name= row[0] + ' ' + row[1], photo= row[2]) for row in cur.fetchall()]

    return render_template('group.html', groupname=groupname, posts=posts, photos=photos)

#Lets us run this as a standalone application
if __name__ == '__main__':
    app.debug = False
    app.run(host='0.0.0.0')
